<?php
/**
 * Created by PhpStorm.
 * User: lozan
 * Date: 03/10/2018
 * Time: 20:49
 */
?>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Pangea demo</title>
    <link rel="stylesheet" type="text/css" href="css/index.css" />
    <link rel="stylesheet" type="text/css" href="css/fonts.css" />
</head>
<body>
<div class="dvContenedorGeneral">
    <div class="dvMenuSuperior">
        menu
        <img class="iconoMenuRight" src="img/icons/person.png" />
    </div>
    <section class="dvContent">
        <div class="dvColLeft">
            <div class="dvResumentPerfil">
                <div class="dvImagenPerfil">
                    <img src="img/fotoyo.jpg" />
                </div>
                <div class="dvDatosPerfil">
                    <div class="dvNombreUsuario">Iván Lozano</div>
                    <div class="dvHashtagUsuario">@ivanlozano</div>
                    <div class="dvNivel">
                        <div class="dvNivelLabel">Nivel:</div>
                        <div class="dvNivelValor">87</div>
                    </div>
                    <div class="dvSeguidoresContenedor">
                        <div class="dvSeguidores">
                            <span>Seguidores</span><span>1.7K</span>
                        </div>
                        <div class="dvSeguidos">
                            <span>Seguidos:</span><span>56</span>
                        </div>
                    </div>
                </div>
                <div>hey qué tal</div>
            </div>
        </div>
        <div class="dvColCenter">
            <h3>Titulillo</h3>
            <p>Contenido del muro</p>
        </div>
        <div class="dvColRight">
            <h3>Tendencias</h3>
            <ul>
                <li>José Mota</li>
                <li>Proscojoncio</li>
                <li>Un Tío Guay</li>
            </ul>
        </div>
    </section>
</div>
</body>
</html>
